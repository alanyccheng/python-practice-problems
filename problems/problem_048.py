# Write a function that meets these requirements.
#
# Name:       count_word_frequencies
# Parameters: sentence, a string
# Returns:    a dictionary whose keys are the words in the
#             sentence and their values are the number of
#             times that word has appeared in the sentence
#
# The sentence will contain now punctuation.
#
# This is "case sensitive". That means the word "Table" and "table"
# are considered different words.
#
# Examples:
#    * sentence: "I came I saw I learned"
#      result:   {"I": 3, "came": 1, "saw": 1, "learned": 1}
#    * sentence: "Hello Hello Hello"
#      result:   {"Hello": 3}

def coutn_word_frequencies(sentence):
    count_dictionary = {}
    splited = sentence.split(" ")

    for word in splited:
        if word not in count_dictionary:
            count_dictionary[word] = 1
        elif word in count_dictionary:
            count_dictionary[word] += 1

    return count_dictionary

print(coutn_word_frequencies("I came I saw I learned"))
print(coutn_word_frequencies("Hello Hello Hello"))
print(coutn_word_frequencies("This is\"case sensitive\". That means the word \"Table\" and \"table\" are considered different words."))
print(coutn_word_frequencies("times that word has appeared in the sentence"))





















## FUNCTION PSEUDOCODE
# function count_word_frequencies(sentence):
    # words = split the sentence
    # counts = new empty dictionary
    # for each word in words
        # if the word is not in counts
            # counts[word] = 0
        # add one to counts[word]
    # return counts
