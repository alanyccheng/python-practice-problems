# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lower = False
    upper = False
    digit = False
    special = False
    min = False
    max = False
    for item in password:
        if item.isalpha():
            if item.isupper():
                upper = True
            elif item.islower():
                lower = True
        elif item.isdigit():
            digit = True
        elif item == "$" or item == "!" or item == "@":
            special = True

    if len(password) >= 6:
        min = True

    if len(password) <= 12:
        max = True

    if (lower == True) and (upper == True) and (digit == True) and (special == True) and (min == True) and (max == True):
        return "Password is valid"
    else:
        return "Password is invalid"

print(check_password("a;sldkjfa;lkwej135135WQW"))
print(check_password("asdf"))
print(check_password("123"))
print(check_password("$$"))
print(check_password("abcABC123!"))
