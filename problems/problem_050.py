# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_test(input_list):
    result1 = []
    result2 = []
    if len(input_list) % 2 == 0:
        mid = (len(input_list) // 2)
        result1 = input_list[0:mid]
        result2 = input_list[mid:]
    elif len(input_list) % 2 == 1:
        mid = (len(input_list) // 2) + 1
        result1 = input_list[0:mid]
        result2 = input_list[mid:]
    return result1, result2

print(halve_the_test([1, 2, 3, 4]))
print(halve_the_test([1, 2, 3]))
print(halve_the_test([1, 2, 3, 4, 5, 6, 7, 8, 9]))
