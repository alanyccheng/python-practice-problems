# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    else:
        max = 0
        for item in values:
            if item > max:
                max = item
        return max

number1 = max_in_list([])
number2 = max_in_list([0])
number3 = max_in_list([1, 2, 3, 4, 5, 6])

print(number1)
print(number2)
print(number3)
