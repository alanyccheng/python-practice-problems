# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
        # check if list is empty
    if (len(values) == 0):
        return None
    else:
        sum_of_list = 0
        for item in values:
            sum_of_list += item

        return sum_of_list

print(calculate_sum([1,2,3,4,5]))
print(calculate_sum([1,2,3,4,5,6,7,8,9,10]))
