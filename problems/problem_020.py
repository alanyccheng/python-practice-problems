# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    count_attend = len(attendees_list)
    count_members = len(members_list)
    count_quorum = count_members * 0.5

    if (count_attend >= count_quorum):
        return True
    else:
        return False

attendees_below50_list = ['John', "Jack"]
attendees_over50_list = ['John', "Jackson", "Jason", "Jack"]
attendees_50_list = ['John', "Jackson", "Jason"]
members_all = ['John', "Jackson", "Jason", "Jackel", "Johnson", "Peter"]

print(has_quorum(attendees_below50_list, members_all))
print(has_quorum(attendees_over50_list, members_all))
print(has_quorum(attendees_50_list, members_all))
