# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    required_ingredients = ["flour", "eggs", "oil"]
    for item in required_ingredients:
        if item not in ingredients:
            return False
    return True


print(can_make_pasta(["flour", "eggs", "oil"]))
print(can_make_pasta(["oil", "flour", "eggs"]))
print(can_make_pasta([ "eggs", "oil","flour"]))
print(can_make_pasta(["flour", "water", "oil"]))
print(can_make_pasta(["flour", "water"]))
