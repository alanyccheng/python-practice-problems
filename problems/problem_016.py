# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    if (x >= 0) and (x <= 10) and (y >= 0) and (y <= 10):
        return True
    else:
        return False

test_x = input("Please input value for x: ")
test_x = int(test_x)
test_y = input("Please input value for y: ")
test_y = int(test_y)

print(is_inside_bounds(test_x, test_y))
