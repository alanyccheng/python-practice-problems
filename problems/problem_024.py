# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    # check if list is empty
    if (len(values) == 0):
        return None
    else:
        list_length = len(values)
        sum_of_list = 0


        for item in values:
            sum_of_list += item

        average_of_list = sum_of_list / list_length
        return average_of_list

print(calculate_average([1,2,3,4,5]))
print(calculate_average([1,2,3,4,5,6,7,8,9,10]))
