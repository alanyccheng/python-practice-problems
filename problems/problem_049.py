# Write a function that meets these requirements.
#
# Name:       sum_two_numbers
# Parameters: two numerical parameters
# Returns:    the sum of the two numbers
#
# Examples:
#    * x: 3
#      y: 4
#      result: 7

def sum_two_numbers(number1, number2):
    return number1 + number2

print(sum_two_numbers(1, 2))
print(sum_two_numbers(3, 4))
print(sum_two_numbers(5,6))
