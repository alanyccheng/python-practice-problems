# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    if (len(values) == 0):
        return None
    else:
        culmulative_score = 0
        for subject_score in values:
            culmulative_score += subject_score

        average_score = culmulative_score / len(values)

        if (average_score >= 90):
            return "A"
        elif (average_score >= 80) and (average_score < 90):
            return "B"
        elif (average_score >= 70) and (average_score < 80):
            return "C"
        elif (average_score >= 60) and (average_score < 70):
            return "D"
        else:
            return "F"




print(calculate_grade([100,100,100,99]))

print(calculate_grade([90, 88, 88]))

print(calculate_grade([80, 79, 79]))

print(calculate_grade([70, 69, 69]))

print(calculate_grade([60, 59, 59]))
