# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

        # max_value = values[0]
        # second_max = values[0]
        # for item in values:
        #     if item > max_value:
        #         second_max = max_value
        #         max_value = item
        #     elif item > second_max:
        #         second_max = item

def find_second_largest(values):
    if len(values) == 0 or len(values) == 1:
        return None
    else:
        sorted_list = sorted(values)
        second_max = sorted_list[-2]
        print(second_max)


find_second_largest([1,2,3,4,5,6,7,8,9,10])
find_second_largest([12,2,3,4,5,6,7,8,9,10])
find_second_largest([144,2,3,4,523,6,7,48,9,10])
find_second_largest([1567,2,3,4,85,6,7,8,9,10])
find_second_largest([133457,2,3,4,554,6,7,8,9,10])
