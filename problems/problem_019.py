# Complete the is_inside_bounds function which has the
# following parameters:
#   x: the x coordinate to check
#   y: the y coordinate to check
#   rect_x: The left of the rectangle
#   rect_y: The bottom of the rectangle
#   rect_width: The width of the rectangle
#   rect_height: The height of the rectangle
#
# The is_inside_bounds function returns true if all of
# the following are true
#   * x is greater than or equal to rect_x
#   * y is greater than or equal to rect_y
#   * x is less than or equal to rect_x + rect_width
#   * y is less than or equal to rect_y + rect_height

def is_inside_bounds(x, y, rect_x, rect_y, rect_width, rect_height):
    if ( (x >= rect_x)
        and (y >= rect_y)
        and (x <= (rect_x + rect_width))
        and (y <= (rect_y + rect_height))
        ):
        return True
    else:
        return False

test_x = input("Please input value for x: ")
test_x = int(test_x)
test_y = input("Please input value for y: ")
test_y = int(test_y)
test_rect_x = input("Please input value for rect_x: ")
test_rect_x = int(test_rect_x)
test_rect_y = input("Please input value for rect_y: ")
test_rect_y = int(test_rect_y)
test_rect_width = input("Please input value for rect_width: ")
test_rect_width = int(test_rect_width)
test_rect_height = input("Please input value for rect_height: ")
test_rect_height = int(test_rect_height)

print(is_inside_bounds(test_x, test_y, test_rect_x, test_rect_y, test_rect_width, test_rect_height))
